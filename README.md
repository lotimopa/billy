# Presentation

A project based on Symfony 4 to list Tree Genealogy.

## Installation

```bash
git clone git@gitlab.com:delny/billy.git
cd billy
composer install -n
npm install
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate -n
```

## Assets
```bash
npm run dev
```
